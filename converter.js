var csv = require('csv'),
	fs = require('fs'),
	parseArgs = require('minimist'),
	entities = new (require('html-entities').XmlEntities)();


const util = require('util');

var argv = parseArgs(process.argv.slice(2));

var outStr = '';

var csvfile = fs.readFile(argv._[0], 'utf8', function (err,data) {
	if (err) {
		return console.log(err);
	}
	csv.parse(data, function(err, data){

		data.forEach(function (row) {
			outStr += '<data name="' + row[0] + '" xml:space="preserve">\n';
			outStr += '  <value>' + entities.encode(row[1]) + '</value>\n';
			outStr += '</data>\n';
		});

		process.stdout.write(outStr);
	});
});



