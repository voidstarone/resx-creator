# RESX Converter

This will take a CSV export of Kentico's localisation strings and transform it into a format that can be inserted into a `custom.resx` file.


To run the file, make sure you're in the same directory as it and run something like this:

	node converter.js "C:\Users\handsome_user\Downloads\cms_resourcestring_part1.csv" >> "C:\Users\handsome_user\Downloads\cms_resourcestring_part1.xml"

The reason for the 'part1' in the filenames is that Kentico will only export 100 localisation strings at at time.